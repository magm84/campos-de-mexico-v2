var rutaBase = '';
var rutaImg = 'img/';
var altura = $(window).height();
var scroll = 0;
var frameActual = 0;
var imgFondo = null;
var divActivo = null;

const alturaContenedores = () => {
  altura = $(window).height();
  ancho = $(window).width();
  $('html,body').css('max-width', ancho);
  if (ancho <= 1024 || altura <= 480) {
    $('.alto-100-js,section.seccion,.contenido-full').css('height', 'auto').css('min-height', 'auto');
  } else {
    $('.alto-100-js,section.seccion,.contenido-full').css('height', altura).css('min-height', altura);
  }
  if (altura > 600) {
    $('.alto-100-js').css('height', altura);
  }
}

const setupSlide = () => {
  $('#fondo').cycle({
    speed: 1000,
    manualSpeed: 1000,
    fx: 'fade',
    continueAuto: false,
    "slides":"> div"
  });
}

const gotoSlide = () => {


  let encontrado = false;
  let frameActual = 0;
  scroll = $(window).scrollTop();

  $('section.seccion').each(function(i) {

    if (!encontrado) {
      frameActual++;
      if (($(this).offset().top + $(this).height()) >= (scroll + (altura / 2))) {
        encontrado = true;
        let el = $(this);
        $('#fondo').cycle('goto', frameActual-1);
      }
    }

  });/*each*/



  // console.log(`${frameActual} de ${document.querySelectorAll('section.seccion').length}`);
  // $('#contador').html(`${frameActual} de ${document.querySelectorAll('section.seccion').length}`);
  // frameActual = 0;

}

const efectoIntro = () => {
  $('#introSonora div.medio').hover((el) => {
    $('#introSonora div.medio').addClass('blur-suave');
    el.target.classList.remove('blur-suave');
    el.target.classList.add('activo');
  }, (el) => {
    $('#introSonora div.medio').removeClass('blur-suave');
    el.target.classList.remove('activo');
  });
}

const seleccionarHistoria = () => {
  $('#introSonora div.medio').click((el) => {
    let elj = $(el.target);
    console.log(elj.data('historia'));

    $('#contenedor').addClass('no-overflow');
    $("#circuloLoader").addClass('activo');
    $('#introSonoraCont').css('display', 'none');
    setTimeout(() => {
      $("#" + elj.data('historia')).css('display', 'block');
      $("#circuloLoader").removeClass('activo');
      $('#contenedor').removeClass('no-overflow');
    }, 800)
    //$(document).scrollTop( $("#"+elj.data('historia')).offset().top);
  });
}

window.load = () => {
  console.log('carga completa');
}

$(document).ready(() => {

  console.log('document ready');

  alturaContenedores();
  setupSlide();
  //crearMapaMenu();
  efectoIntro();
  //seleccionarHistoria();

  $("#share").click((e) => {
    e.preventDefault();
    console.log('share');
    $("#redesSociales").toggleClass('activo');
  });

  $('#toggle-menu').click((e) => {
    e.preventDefault();
    if (!$("#circuloMenu").hasClass('activo')) {
      $("#circuloMenu").addClass('activo');
      setTimeout(() => {
        $('#menu-desplegable').addClass('active');
      }, 500);
    } else {
      $('#menu-desplegable').removeClass('active');

      setTimeout(() => {
        $("#circuloMenu").removeClass('activo');
      }, 500);

    }

  });

});

$(window).scroll(() => {
  gotoSlide();
});

$(window).resize(() => {
  alturaContenedores();
});
