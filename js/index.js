var rutaBase = '';
var rutaImg = 'img/';
var altura = $(window).height();
var scroll = 0;
var frameActual = 0;
var imgFondo = null;
var divActivo = null;

const alturaContenedores = () => {
  altura = $(window).height();
  ancho = $(window).width();
  $('html,body').css('max-width', ancho);
  $('section.seccion').css('height', altura);
}

const efectoFondo = () => {

  let encontrado = false;

  scroll = $(window).scrollTop();

  $('section.seccion').each(function(i) {

    if (!encontrado) {
      frameActual++;
      if (($(this).offset().top + $(this).height()) >= (scroll + (altura / 2))) {
        encontrado = true;
        let el = $(this);

        if (el.data('img') !== '') {
          $('#fondo').cycle('goto', parseInt(el.data('img')));
        }

        if (el.data('efecto') != '') {
          document.querySelector('#fondo').className = el.data('efecto');
        } else {
          document.querySelector('#fondo').className = '';
        }

      }
    }

  });/*each*/

  console.log(`${frameActual} de ${document.querySelectorAll('section.seccion').length}`);
  $('#contador').html(`${frameActual} de ${document.querySelectorAll('section.seccion').length}`);
  frameActual = 0;

}

const setupSlide = () => {

  $('#fondo').cycle({speed: 1000, manualSpeed: 1000, fx: 'fade', continueAuto: false, "overlayTemplate": "<p>{{desc}}</p>"});

}

const crearMapaMenu = () => {

  d3.json(rutaBase + "js/mx_tj.json", function(error, mx) {

    var estados = [
      {
        "id": 26,
        "nombre": "Sonora",
        "color": "red"
      }, {
        "id": 12,
        "nombre": "guerrero",
        "color": "blue"
      }, {
        "id": 19,
        "nombre": "Nuevo León",
        "color": "purple"
      }, {
        "id": 10,
        "nombre": "Durango",
        "color": "orange"
      }, {
        "id": 11,
        "nombre": "Guanajuato",
        "color": "lime"
      }
    ];

    var width = 1000,
      height = 800;
    active = d3.select(null);

    var projection = d3.geo.mercator().scale(1800).center([-103.34034978813841, 28.012062015793]);

    var svg = d3.select("#menu-mapa").append("svg").attr({
      "viewBox": "0 0 " + width + " " + height,
      "preserveAspectRatio": "xMinYMin meet",
      "width": width,
      "height": height
    });

    var g = svg.append("g");

    var path = d3.geo.path().projection(projection);

    var tooltip = d3.select('.mapa').append('div').attr('class', 'hidden tooltip');

    g.selectAll("path").data(topojson.feature(mx, mx.objects.states).features).enter().append("path").attr("d", path).attr("class", (d) => {
      var estadoTmp = estados.filter(emx => emx.id == d.properties.state_code);
      if (estadoTmp.length > 0) {
        return "estado-valido";
      } else {
        return "estado";
      }

    }).attr("id", function(d) {
      return d.properties.state_code;
    }).attr("data-name", function(d) {
      return d.properties.state_name;
    }).attr("fill", (d) => {
      var estadoTmp = estados.filter(emx => emx.id == d.properties.state_code);
      if (estadoTmp.length > 0) {
        return estadoTmp[0].color;
      } else {
        return "#fff";
      }
    }).on('click', (d) => {
      console.log(d.properties);
    });

  });

}

window.load = () => {
  console.log('carga completa');
}

$(document).ready(() => {

  console.log('document ready');

  alturaContenedores();
  setupSlide();
  efectoFondo();
  crearMapaMenu();

  $('#bocina').click((e) => {
    e.preventDefault();

  });

  $("#share").click((e) => {
    e.preventDefault();
    console.log('share');
    $("#redesSociales").toggleClass('activo');
  });

  $('#toggle-menu').click((e) => {
    e.preventDefault();
    if (!$("#circuloMenu").hasClass('activo')) {
      $("#circuloMenu").addClass('activo');
      setTimeout(() => {
        $('#menu-desplegable').addClass('active');
      }, 500);
    } else {
      $('#menu-desplegable').removeClass('active');

      setTimeout(() => {
        $("#circuloMenu").removeClass('activo');
      }, 500);

    }

  });

});

$(window).scroll(() => {
  efectoFondo();
});

$(window).resize(() => {
  alturaContenedores();
});
